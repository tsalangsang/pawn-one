<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pawn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#eyBK3xEa0,jJm&/y`u))q:MPjI:!FV)5tM}6]}lO+C,7:Z-CogwSg{slx5u-~>i');
define('SECURE_AUTH_KEY',  '({|E_[NYX-E~Diz1vG:Gf@OBQ*LoG4mp[{Pfmj_Fon0Yf4Zkqc@#x-fjZm`>y @s');
define('LOGGED_IN_KEY',    'H9s<)oy(Dn9K1NT*kEb,l?9vcTvo7>6v0?9+HSTCm?^vd=BH:4}p_@%jXA4:?xfu');
define('NONCE_KEY',        '-8O=+5esNe`:R uAoVI$+zZ5*)Ga4tv2Z3W6gx!H(>n=5C TC@2xW]m#ym|!xu~>');
define('AUTH_SALT',        'lb(_ubHugh4aTVQr4u#rGi2H)K?sIOwHhxurt!4~pTsYt1Yil #T U)A /_nq[0X');
define('SECURE_AUTH_SALT', '1nr-?R3{3bfq,ESo#bYa1A!mgq@O3M:DQAk>Fd2W}7l;a=U{HT{$`vU81#6uR7!y');
define('LOGGED_IN_SALT',   'ki)6Vtee; ICbTV]vN5(!K^X9WIrHA:,c{[Ki$f6CW+@[T*RY2 KHAce}+p4@ Q)');
define('NONCE_SALT',       'C{I{n7Sqbw(.9MS_|:y4h$1FrY!jnWT ~1P]IL:tpiT^=Xb|g5bG1m[SCyYNG~jg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
